// Use cpp to scan a file and print line numbers.
// Print out each input line read in, then strtok it for
// tokens.

#include <string>
#include <iostream>
#include <fstream>
using namespace std;

#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <unistd.h>
#include <libgen.h>

#include "auxlib.h"
#include "stringset.h"
#include "lyutils.h"
#include "yyparse.h"
#include "astree.h"

const string CPP = "/usr/bin/cpp";
FILE* output_tok;
FILE* astfile;

int main (int argc, char** argv) {
   set_execname (argv[0]);
   int c;
   char* dvalue = NULL;
   yy_flex_debug = 0;
   yydebug = 0;
   while ((c = getopt(argc, argv, "ly@:D:")) != -1) {
     switch (c) {
      case 'l':
        yy_flex_debug = 1;
        break;
      case 'y':
        yydebug = 1;
        break;
      case '@':
        set_debugflags(optarg);
        break;
      case 'D':
        dvalue = optarg;
        break;
      case '?':
        errprintf("Invalid parameters\n");
        break;
      default:
        break;
     }
   }
   for (int argi = optind; argi < argc; ++argi) {
      char* filename = argv[argi];
      char* bname = basename(filename);
      if (access(filename, R_OK) < 0) {
        errprintf("file not found, aborting\n");
        return get_exitstatus();
      }
      if(strcmp(bname + strlen(bname) - 3, ".oc")) {
        errprintf("input file must have .oc extension, aborting\n");
        return get_exitstatus();
      }
      string command;
      if (dvalue != NULL) {
        command = CPP + " " + dvalue + " " + filename;
     }else {
        command = CPP + " " + filename;
     }

      //printf ("command=\"%s\"\n", command.c_str());
      yyin = popen (command.c_str(), "r");
      if (yyin == NULL) {
         errprintf (command.c_str());
         return get_exitstatus();
      }else {
         char* pch = strrchr(bname, '.');
         strcpy(pch+1, "str");
         ofstream strfile;
         strfile.open(bname);
         strcpy(pch+1, "tok");
         output_tok = fopen(bname, "w");
         strcpy(pch+1, "ast");
         astfile = fopen(bname, "w");
         if (yyparse() != 0) {
           errprintf("parsing failed, exiting\n");
           return get_exitstatus();
         }
         int pclose_rc = pclose (yyin);
         eprint_status (command.c_str(), pclose_rc);
         dump_stringset (strfile);
         dump_astree (astfile, yyparse_astree);
         strfile.close();
         fclose(output_tok);
         fclose(astfile);
      }
   }
   return get_exitstatus();
}
