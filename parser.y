%{ 
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "lyutils.h"
#include "astree.h"

%}

%debug
%defines
%error-verbose
%token-table
%verbose

%token TOK_VOID TOK_BOOL TOK_CHAR TOK_INT TOK_STRING
%token TOK_IF TOK_ELSE TOK_WHILE TOK_RETURN TOK_STRUCT
%token TOK_FALSE TOK_TRUE TOK_NULL TOK_NEW TOK_ARRAY
%token TOK_ORD TOK_CHR
%token TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%token TOK_IDENT TOK_INTCON TOK_CHARCON TOK_STRINGCON

%token TOK_BLOCK TOK_CALL TOK_IFELSE TOK_VARDECL
%token TOK_POS TOK_NEG TOK_TYPEID TOK_FIELD TOK_INDEX
%token TOK_FUNCTION TOK_PARAMLIST TOK_PROTOTYPE
%token TOK_NEWARRAY TOK_NEWSTRING TOK_RETURNVOID
%token TOK_ROOT
%token TOK_DECLID
// TODO Replace INITDECL w/ VARDECL

%right     TOK_IF TOK_ELSE
%right     '='
%left      TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%left      '+' '-'
%left      '*' '/' '%'
%right     TOK_POS TOK_NEG '!' TOK_NEW TOK_ORD TOK_CHR
%left      '[' '.' TOK_CALL
%nonassoc  '('

%start start

%%
// GOOD
start     : program                { yyparse_astree = $1; }
          ;
program   : program structdef      { $$ = adopt1 ($1, $2); }
          | program function       { $$ = adopt1 ($1, $2); }
          | program statement      { $$ = adopt1 ($1, $2); }
          | program error '}'      { $$ = $1; }
          | program error ';'      { $$ = $1; }
          |                        { $$ = new_parseroot(); }
          ;
// GOOD (modified)
structdef : TOK_STRUCT TOK_IDENT '{' '}'
          { $$ = adopt1 ($1, setsym($2, TOK_TYPEID));
            free_ast2 ($3, $4); }
          | stritems '}'           { free_ast ($2); $$ = $1; }
          ;
// GOOD doesnt appear on listing (modified)
stritems  : stritems fielddecl ';' { free_ast($3); 
                                     $$ = adopt1 ($1, $2); }
          | TOK_STRUCT TOK_IDENT '{' fielddecl ';'
          { free_ast2 ($3, $5); 
            $$ = adopt2 ($1, setsym($2, TOK_TYPEID), $4); }
          ;
// ADDED 
fielddecl : basetype TOK_ARRAY TOK_IDENT
          { $$ = adopt2 ($2, $1, setsym($3, TOK_FIELD)); }
          | basetype TOK_IDENT     { $$ = adopt1 ($1, $2); }
          ;
// GOOD (reviewed doc)
identdecl : basetype TOK_ARRAY TOK_IDENT
          { $$ = adopt2 ($2, $1, setsym($3, TOK_DECLID)); }
          | basetype TOK_IDENT     { $$ = adopt1 ($1, 
                                     setsym($2, TOK_DECLID)); }
          ;
// GOOD
basetype  : TOK_VOID               { $$ = $1; }
          | TOK_BOOL               { $$ = $1; }
          | TOK_CHAR               { $$ = $1; }
          | TOK_INT                { $$ = $1; }
          | TOK_STRING             { $$ = $1; }
          | TOK_IDENT
          { $$ = setsym ($1, TOK_TYPEID); }
          ;
// GOOD
function  : identdecl '(' ')' ';'
          { free_ast2 ($3, $4);
               $$ = adopt2 (new_protonode (), $1,
                         setsym ($2, TOK_PARAMLIST) ); }
          | identdecl params ')' ';'
          { $$ = adopt2 (new_protonode(), $1, $2); free_ast2($3, $4); }
          | innerfunc '}'            { free_ast ($2); $$ = $1; }
          ;
// GOOD doesnt appear on listing 
innerfunc : identdecl '(' ')' '{'
          { free_ast ($3); 
               $$ = adopt3 (new_funcnode ($1), $1,
                         setsym ($2, TOK_PARAMLIST),
                         setsym ($4, TOK_BLOCK) ); }
          | identdecl '(' ')' blockmore
          { free_ast ($3);
               $$ = adopt3 (new_funcnode ($1), $1,
                         setsym ($2, TOK_PARAMLIST), $4); }
          | identdecl params ')' '{'
          { free_ast ($3);
               $$ = adopt3 (new_funcnode ($1), $1, $2,
                         setsym ($4, TOK_BLOCK) ); }
          | identdecl params ')' blockmore
          { free_ast ($3);
               $$ = adopt3 (new_funcnode ($1), $1, $2, $4); }
          ;
// GOOD doesnt appear on listing
params    : params ',' identdecl
          { free_ast ($2); $$ = adopt1 ($1, $3); }
          | '(' identdecl
          { $$ = adopt1sym ($1, $2, TOK_PARAMLIST); }
          ;
// GOOD
block     : '{' '}'
          {  free_ast ($2); $$ = setsym ($1, TOK_BLOCK); }
          | blockmore '}'          { free_ast ($2); $$ = $1; }
          | ';'                    { $$ = $1; }
          ;
// GOOD doesnt appear on listing
blockmore : blockmore statement    { $$ = adopt1 ($1, $2); }
          | '{' statement
          { $$ = adopt1( setsym($1, TOK_BLOCK), $2) ;}
          ;
// GOOD (modified)
statement : block                  { $$ = $1; }
          | vardecl                { $$ = $1; }
          | while                  { $$ = $1; }
          | ifelse                 { $$ = $1; }
          | return                 { $$ = $1; }
          | expr ';'               { free_ast ($2); $$ = $1; }
          ;
// GOOD (modified)
vardecl  : identdecl '=' expr ';'
          { free_ast ($4);
               $$ = adopt2( setsym($2, TOK_VARDECL), $1, $3); }
          ;
// GOOD (modified)
while     : TOK_WHILE '(' expr ')' statement
          { free_ast2 ($2, $4); $$ = adopt2 ($1, $3, $5); }
          ;
// GOOD (modified)
ifelse    : TOK_IF '(' expr ')' statement %prec TOK_ELSE
          { free_ast2 ($2, $4); $$ = adopt2 ($1, $3, $5); }
          | TOK_IF '(' expr ')' statement TOK_ELSE statement
          { free_ast3 ($2, $4, $6);
               $$ = adopt2 (adopt1sym ($1, $3, TOK_IFELSE), $5, $7); }
          ;
// GOOD
return    : TOK_RETURN expr ';'
          { free_ast ($3); $$ = adopt1 ($1, $2); }
          | TOK_RETURN ';'
          { free_ast ($2); $$ = setsym ($1, TOK_RETURNVOID); }
          ;
// GOOD
expr      : expr '=' expr          { $$ = adopt2 ($2, $1, $3); }
          | expr TOK_EQ expr       { $$ = adopt2 ($2, $1, $3); }
          | expr TOK_NE expr       { $$ = adopt2 ($2, $1, $3); }
          | expr TOK_LT expr       { $$ = adopt2 ($2, $1, $3); }
          | expr TOK_LE expr       { $$ = adopt2 ($2, $1, $3); }
          | expr TOK_GT expr       { $$ = adopt2 ($2, $1, $3); }
          | expr TOK_GE expr       { $$ = adopt2 ($2, $1, $3); }
          | expr '+' expr          { $$ = adopt2 ($2, $1, $3); }
          | expr '-' expr          { $$ = adopt2 ($2, $1, $3); }
          | expr '*' expr          { $$ = adopt2 ($2, $1, $3); }
          | expr '/' expr          { $$ = adopt2 ($2, $1, $3); }
          | expr '%' expr          { $$ = adopt2 ($2, $1, $3); }
          | '+' expr %prec TOK_POS { $$ = adopt1sym ($1, $2, TOK_POS); }
          | '-' expr %prec TOK_NEG { $$ = adopt1sym ($1, $2, TOK_NEG); }
          | '!' expr               { $$ = adopt1 ($1, $2); }
          | TOK_ORD expr           { $$ = adopt1 ($1, $2); }
          | TOK_CHR expr           { $$ = adopt1 ($1, $2); }
          | allocator              { $$ = $1; }
          | call                   { $$ = $1; }
          | variable               { $$ = $1; }
          | constant               { $$ = $1; }
          | '(' expr ')'           { free_ast2 ($1, $3); $$ = $2; }
          ;
// GOOD
allocator : TOK_NEW TOK_IDENT '(' ')'
          { free_ast2 ($3, $4);
               $$ = adopt1 ($1, setsym ($2, TOK_TYPEID) ); }
          | TOK_NEW TOK_STRING '(' expr ')'
          { free_ast3 ($2, $3, $5);
               $$ = adopt1sym ($1, $4, TOK_NEWSTRING); }
          | TOK_NEW basetype '[' expr ']'
          { free_ast2 ($3, $5); 
               $$ = adopt1 (adopt1sym ($1, $2, TOK_NEWARRAY), $4); }
          ;
// GOOD
call      : TOK_IDENT '(' ')'
          { free_ast ($3); $$ = adopt1sym ($2, $1, TOK_CALL); }
          | callmore ')'            { free_ast ($2); $$ = $1; }
          ;
// GOOD doesnt appear in listing
callmore   : callmore ',' expr
          { free_ast ($2); $$ = adopt1 ($1, $3); }
          | TOK_IDENT '(' expr
          { $$ = adopt1 (adopt1sym ($2, $1, TOK_CALL), $3); }
          ;
// GOOD
variable  : TOK_IDENT              { $$ = $1; }
          | expr '[' expr ']'
          { free_ast ($4);
               $$ = adopt1 (adopt1sym ($2, $1, TOK_INDEX), $3); }
          | expr '.' TOK_IDENT
          { $$ = adopt2 ($2, $1, setsym($3, TOK_FIELD)); }
          ;
// GOOD
constant  : TOK_INTCON             { $$ = $1; }
          | TOK_CHARCON            { $$ = $1; }
          | TOK_STRINGCON          { $$ = $1; }
          | TOK_FALSE              { $$ = $1; }
          | TOK_TRUE               { $$ = $1; }
          | TOK_NULL               { $$ = $1; }
          ;

%%

const char *get_yytname (int symbol) {
   return yytname [YYTRANSLATE (symbol)];
}
 
bool is_defined_token (int symbol) { 
   return YYTRANSLATE (symbol) > YYUNDEFTOK; 
} 
/* 
static void* yycalloc (size_t size) { 
   void* result = calloc (1, size); 
   assert (result != NULL); 
   return result; 
} 

*/ 
